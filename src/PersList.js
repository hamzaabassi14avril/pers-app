import { Link, useHistory } from "react-router-dom";
const PersList = ({personnes, title}) => {
    let history = useHistory();
    const handleClick = (e) => {
        let id = e.target.parentElement.getAttribute('data-id');
        console.log(e.target.parentElement);
        fetch('http://localhost:8000/personnes/' + id, {
            method: 'DELETE'
        }).then(() => {
            history.push('/');
        })
    }
    return (
        <div className="personne-list">
            <h2>{title}</h2>
            {personnes.map((personne) => (
                <div className="personne-wrapper" key={personne.id}>
					<div className="actions">
                        <Link to={`/pers/${personne.id}`} className="edit"><i className="icofont-ui-edit"></i></Link>
                        <button onClick={ handleClick } data-id={personne.id} className="del"><i className="icofont-ui-delete"></i></button>
                    </div>
                    <h2 className="title">{personne.prenom} {personne.nom}</h2>
                    <div className="details">
                        <span><strong>Tel: </strong>{ personne.tel }</span>
                        <span><strong>Sexe: </strong>{ personne.sexe }</span>
                        <span><strong>Ville: </strong>{ personne.ville }</span>
                    </div>
                </div>
            ))}
        </div>
    );
}
export default PersList;
