import {Link} from 'react-router-dom';
const Menu = () => {
    return ( 
        <nav className="menu">
            <h1>Pers <span style={{color: '#0999b3'}}>App</span></h1>
            <div className="links">
                <Link to="/" style={{color: 'white', backgroundColor: '#f1356d'}}>Accueil</Link>
                <Link to="/create">Nouvelle Personne</Link>
            </div>
        </nav>
    );
}
 
export default Menu;