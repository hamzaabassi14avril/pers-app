import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import db from "./useFirebase";

const Create = () => {
    const [nom, setNom] = useState();
    const [prenom, setPrenom] = useState();
    const [tel, setTel] = useState();
    const [sexe, setSexe] = useState('M');
    const [ville, setVille] = useState('New York');
    const [isPending, setIsPending] = useState(false);
    const history = useHistory();
    
    const handleSubmit = (e) => {
        e.preventDefault();
        db.collection('personnes').add({
            nom: nom,
            prenom: prenom,
            tel: tel,
            sexe: sexe,
            ville: ville
        }) 
        e.target.reset();
        history.push('/'); 
    }
    return (
        <div className="create">
            <h2>Ajout d'une personne</h2>
            <form onSubmit={ handleSubmit }>
                <label htmlFor="">Nom:</label>
                <input 
                    type="text" 
                    required 
                    value={ nom }
                    onChange={(e) => setNom(e.target.value)}
                />
                <label htmlFor="">Prenom:</label>
                <input 
                    type="text" 
                    required 
                    value={ prenom }
                    onChange={(e) => setPrenom(e.target.value)}
                />
                <label htmlFor="">Tel:</label>
                <input 
                    type="text" 
                    required 
                    value={ tel }
                    onChange={(e) => setTel(e.target.value)}
                />
                <label htmlFor="">Sexe:</label>
                <select
                    value={ sexe }
                    onChange={(e) => setSexe(e.target.value)}
                >
                    <option value="Masculin">Masculin</option>
                    <option value="Feminin">Feminin</option>
                </select>
                <label htmlFor="">Ville:</label>
                <select
                    value={ ville }
                    onChange={(e) => setVille(e.target.value)}
                >
                    <option value="New York">New York</option>
                    <option value="Miami">Miami</option>
                    <option value="Yaoundé">Yaoundé</option>
                    <option value="Douala">Douala</option>
                    <option value="Ngaoundéré">Ngaoundéré</option>
                </select>
                { !isPending && <button>Enregistrer la personne</button>}
                { isPending && <button disabled>Adding Blog...</button>}
            </form>
        </div>
    );
}
 
export default Create;