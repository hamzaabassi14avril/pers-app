import Menu from './Menu';
import Accueil from './Accueil';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Create from './Create';
import PersDetails from './PersDetails';
import Error404 from './Error404';

function App(){
  return (
    <Router>
      <div className="App">
        <Menu />
        <div className="content">
          <Switch>
            <Route exact path="/">
              <Accueil />
            </Route>
            <Route path="/create">
              <Create />
            </Route>
            <Route path="/pers/:id">
              <PersDetails />
            </Route>
            <Route path="*">
              <Error404 />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
