import db from "./useFirebase";
import { Link, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
const Accueil = () => {
    const [personnes, setPersonnes] = useState([]);
    useEffect(() => {
        db.collection('personnes').onSnapshot(snapshot => {
            const data = snapshot.docs.map(doc => {return {id: doc.id, data: doc.data()}});
            setPersonnes(data);
        });
    }, []);
    const handleDelete = (e) => {
        e.preventDefault();
        let id = e.target.parentElement.getAttribute('data-id');
        db.collection('personnes').doc(id).delete();
    }
    return ( 
        <div className="personne-list">
            <h2>Liste des personnes</h2>
            {personnes.map((personne) => (
                <div className="personne-wrapper" key={personne.id}>
					<div className="actions">
                        <Link to={`/pers/${personne.id}`} className="edit"><i className="icofont-ui-edit"></i></Link>
                        <button data-id={personne.id} onClick={handleDelete} className="del"><i className="icofont-ui-delete"></i></button>
                    </div>
                    <h2 className="title">{personne.data.prenom} {personne.data.nom}</h2>
                    <div className="details">
                        <span><strong>Tel: </strong>{ personne.data.tel }</span>
                        <span><strong>Sexe: </strong>{ personne.data.sexe }</span>
                        <span><strong>Ville: </strong>{ personne.data.ville }</span>
                    </div>
                </div>
            ))}
        </div>
    );
}
export default Accueil;