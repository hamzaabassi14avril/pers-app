import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import db from "./useFirebase";

const PersDetails = () => {
    const [error, setError] = useState();
    const [isPending, setIsPending] = useState(true);
    const { id } = useParams();
    const [pers, setPers] = useState([]);
    const [nom, setNom] = useState();
    const [prenom, setPrenom] = useState();
    const [tel, setTel] = useState();
    const [sexe, setSexe] = useState();
    const [ville, setVille] = useState();

    const history = useHistory();

    useEffect(() => {
        db.collection("personnes").doc(id).get().then((doc) => { 
            setPers(doc.data());
            setIsPending(false);
            setNom(doc.data().nom);
            setPrenom(doc.data().prenom);
            setTel(doc.data().tel);
            setSexe(doc.data().sexe);
            setVille(doc.data().ville);
        }).catch((error) => {
            console.log(error);
        })
    }, []);
    
    const handleSubmit = (e) => {
        e.preventDefault();
        let form = e.target;
        db.collection('personnes').doc(id).update({
            nom: nom,
            prenom: prenom,
            tel: tel,
            sexe: sexe,
            ville: ville
        }) 
        e.target.reset();
        history.push('/'); 
    }

    return (
        <div className="pers-details">
            { isPending && <div>Loading...</div> }
            { error && <div>{ error }</div> }
            { !isPending && pers && (
                <div className="create">
                    <h2>Modification d'une personne</h2>
                    <form onSubmit={handleSubmit}>
                        <label htmlFor="">Nom:</label>
                        <input 
                            type="text"
                            required 
                            value={ nom }
                            onChange={(e) => setNom(e.target.value)}
                        />
                        <label htmlFor="">Prenom:</label>
                        <input 
                            type="text" 
                            required 
                            value={ prenom }
                            onChange={(e) => setPrenom(e.target.value)}
                        />
                        <label htmlFor="">Tel:</label>
                        <input 
                            type="text"
                            required 
                            value={ tel }
                            onChange={(e) => setTel(e.target.value)}
                        />
                        <label htmlFor="">Sexe:</label>
                        <select
                            value={ sexe }
                            onChange={(e) => setSexe(e.target.value)}
                        >
                            <option value="M">Masculin</option>
                            <option value="F">Feminin</option>
                        </select>
                        <label htmlFor="">Ville:</label>
                        <select
                            value={ ville }
                            onChange={(e) => setVille(e.target.value)}
                        >
                            <option value="New York">New York</option>
                            <option value="Miami">Miami</option>
                            <option value="Yaoundé">Yaoundé</option>
                            <option value="Douala">Douala</option>
                            <option value="Ngaoundéré">Ngaoundéré</option>
                        </select>
                        { !isPending && <button>Modifier la personne</button>}
                        { isPending && <button disabled>Adding Blog...</button>}
                    </form>
                </div>
            ) }
        </div>
    );
}

export default PersDetails;